﻿using Lesson10.Task1;
using Lesson10.Task2;
using Lesson10.Task2.Engine;

internal class Program
{
    private static void Main(string[] args)
    {
        //Task1
        /*
             1. Напишите обобщенный класс, который может хранить в массиве объекты любого типа.
            Кроме того, данный класс должен иметь методы для добавления данных в массив, удаления из массива,
            получения элемента из массива по индексу и метод, возвращающий длину массива.
        */

        GenericArray<string> array1 = new GenericArray<string>();
        GenericArray<int> array2 = new GenericArray<int>();

        //Добавить элементы в массив
        array1.AddDataToArray("Petya", "Vasya", "Misha", "Pasha", "Dasha");
        array2.AddDataToArray(2, 36, 953, 9467, 0);

        //Удалить элемент из массива
        array1.RemoveDataToArray(0);
        array2.RemoveDataToArray(0);

        //Получить элемент из массива
        var el1 = array1.GetArrayElement(1);
        var el2 = array2.GetArrayElement(4);

        //Получить длину массива
        var lengthArray1 = array1.GetArrayLength();
        var lengthArray2 = array2.GetArrayLength();

        //Task2
        /*
             2. Реализовать класс машина у который будет поле  обобщенное двигатель.
            Создать иерархию наследования для двигателей (абстрактный, дизельный, бензиновый, електро).
            Сделать так чтобы создать автомобиль можно было только передавая туда один из типов двигателя.
            Реализовать методы для движения автомобиля и заправки в зависимости от типа двигателя.
        */

        BenzineEngine benzineEngine = new BenzineEngine();
        ElectroEngine electroEngine = new ElectroEngine();
        Car<BenzineEngine> car1 = new Car<BenzineEngine>(benzineEngine, 50);
        Car<ElectroEngine> car2 = new Car<ElectroEngine>(electroEngine, 50);

        //Заправка автомобиля
        car1.FillTheCar(40);
        car2.FillTheCar(100);

        //Движение автомобиля
        car1.MoveCar(100);
        car2.MoveCar(200);
    }
}