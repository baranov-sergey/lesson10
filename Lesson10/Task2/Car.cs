﻿using Lesson10.Task2.Engine;

namespace Lesson10.Task2
{
    /*
     2. Реализовать класс машина у который будет поле  обобщенное двигатель.
    Создать иерархию наследования для двигателей (абстрактный, дизельный, бензиновый, електро).
    Сделать так чтобы создать автомобиль можно было только передавая туда один из типов двигателя.
    Реализовать методы для движения автомобиля и заправки в зависимости от типа двигателя.
     */

    internal class Car<T> where T : BaseEngine
    {
        public T Engine { get; }
        public int CapacityFuel { get; }
        public int QtFuel { get; protected set; }

        public Car(T engine, int capacityFuel)
        {
            this.Engine = engine;
            this.CapacityFuel = capacityFuel;
        }

        public void MoveCar(int distance)
        {
            int fuelConsumptionPerTrip = distance / (100 / Engine.FuelConsumption);
            if (fuelConsumptionPerTrip <= QtFuel)
            {
                Console.WriteLine($"{Engine.FuelConsumption} fuel will be spent per trip");
                QtFuel -= fuelConsumptionPerTrip;
            }
            else
            {
                Console.WriteLine($"Not enough {Engine.FuelConsumption} fuel for the trip");
            }
        }

        public void FillTheCar(int fuel)
        {
            if (CapacityFuel < QtFuel + fuel)
            {
                Console.WriteLine("Capacity is less than the amount of fuel for refueling");
            }
            else
            {
                QtFuel += fuel;
                Console.WriteLine($"Engine type: {Engine.MotorFuel}, amount of fuel: {QtFuel}");
            }
        }
    }
}
