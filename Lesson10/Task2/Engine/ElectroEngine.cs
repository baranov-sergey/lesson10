﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson10.Task2.Engine
{
    internal class ElectroEngine : BaseEngine
    {
        protected const string motorFuel = "charge";
        protected const int chargeConsumption = 150;          //Вт на 100км
        public ElectroEngine() : base(motorFuel, chargeConsumption)
        { }
    }
}
