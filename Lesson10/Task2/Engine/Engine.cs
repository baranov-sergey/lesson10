﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson10.Task2.Engine
{
    internal abstract class BaseEngine
    {
        public string MotorFuel { get; } = string.Empty;
        public int FuelConsumption { get; } = default;

        public BaseEngine(string motorFuel, int fuelConsumption)
        {
            MotorFuel = motorFuel;
            FuelConsumption = fuelConsumption;
        }
    }
}
