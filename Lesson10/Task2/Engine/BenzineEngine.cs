﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson10.Task2.Engine
{
    internal class BenzineEngine : BaseEngine
    {
        protected const string motorFuel = "benzine";
        protected const int fuelConsumption = 10;       //на 100км

        public BenzineEngine() : base(motorFuel, fuelConsumption)
        { }
    }
}
