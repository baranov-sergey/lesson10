﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson10.Task2.Engine
{
    internal class DieselEengine : BaseEngine
    {
        protected const string motorFuel = "diesel fuel";
        protected const int fuelConsumption = 20;          //на 100км
        public DieselEengine() : base(motorFuel, fuelConsumption)
        { }
    }
}
