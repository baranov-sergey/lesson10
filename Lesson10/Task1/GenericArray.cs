﻿

using System;
using System.Drawing;

namespace Lesson10.Task1
{
    /*
     1. Напишите обобщенный класс, который может хранить в массиве объекты любого типа.
    Кроме того, данный класс должен иметь методы для добавления данных в массив, удаления из массива,
    получения элемента из массива по индексу и метод, возвращающий длину массива.
     */

    internal class GenericArray<T>
    {
        private T[] array;
        private const int size = 5;
        private int cnt = 0;

        public GenericArray()
        {
            array = new T[size];
        }

        public GenericArray(int size)
        {
            array = new T[size];
        }

        public void AddDataToArray(params T[] collection)
        {
            if ((collection.Length + cnt) >= array.Length)
            {
                Array.Resize(ref array, array.Length * 2);
            }

            foreach (var item in collection)
            { 
                array[cnt] = item;
                cnt++;
            }
        }

        public void RemoveDataToArray(int index)
        {
            array[index] = default;
        }

        public T GetArrayElement(int index)
        {
            return array[index];
        }

        public int GetArrayLength()
        {
            return array.Length;
        }
    }
}
